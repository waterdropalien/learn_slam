# SLAM练习开发项目

通过如下一步一步的练习，从简单识别红色的圈、并结合VINS识别的位置，实现无人机穿红色的圈；模拟UWB的数据采集，实现UWB的数据优化，从而学会多数据源的融合。


![cover](images/uav_circle.jpeg)



## 1. 练习目标

* 第一阶段：让一个无人机自主钻过一个红色的圈

    * 不需要GPS，能够实现自主定位
    * 可以使用UWB的LP模式进行定位
    * 使用RealSense+VINS+NX实现自主定位
    * 识别红色的圈，定位，并自主控制飞机飞行
    * 为降低实验成本，先在AirSim等仿真环境实现整体的控制

* 第二阶段：多个无人机能够自主决策以最快的速度穿过多个红色的圈

    * 以什么样的策略分布式扫描场地，并定位每个圈的位置

    * 基于圈的位置，优化每个飞机的轨迹、时间等

    * 无人机集群使用UWB的DR模式

    * 设计强化学习策略，实现无模型的控制

    * 多个无人机的通信方式
    
        

## 2. 技术点

* [仿真架构](https://gitee.com/pi-lab/research_uav_cv_simulation)
* 阶段1的仿真环境构建：基于设计的仿真系统架构，能够在仿真环境实现第一阶段所有的功能
* 圈的识别、定位：识别红色的圈，并解算其位置
* UWB数据融合 （解决高度漂移问题）：将UWB数据和VINS（激光测高计）等数据进行融合，提高定位的稳定性
* VINS：在NX上能够稳定运行VINS
* RealSense数据读取
* [NX配置](https://gitee.com/pi-lab/research_flight_controller/tree/master/hardware/NX)
* 系统整合、实验



## 3. 任务分解

- [ ] 使用UWB局部定位模式，可同时配合激光测距模块，控制无人机在UWB的定位下完成画圆或直线飞行操作
- [ ] 完成VINS在Realense，NX上面的稳定运行，达到一定的定位精度，能够完成真机实验
- [ ] 使用VIO定位数据，完成VIO定位下的自主画圆或特定轨迹飞行
- [ ] 视觉处理能较好的识别出在户外真实情况下红圈的圆心位置。（为了使飞行正对圆圈飞行，可能要识别圆圈的圆心位置，然后调整无人机位姿后再进行穿圆。这一步是否也可以通过判断图像中圆的偏心率来判断是否进行穿圆或先进行调整）
- [ ] 仿真中完成无人机穿圈实验
- [ ] 分别完成VIO、UWB定位下无人机穿圈实验
- [ ] 仿真完成UWB集群无人机相对定位的实验验证，尺度上的问题考虑是否可以通过GPS数据或其他带有尺度的传感器解决
- [ ] 完成两架无人机先后穿过红圈的真机实验



## 参考资料

* [一步一步学飞控](https://gitee.com/pi-lab/research_flight_controller)
* [飞行器 - 环境 - 一体化仿真系统](https://gitee.com/pi-lab/research_uav_cv_simulation)
* [2022 Swarm of micro flying robots in the wild](https://www.science.org/doi/10.1126/scirobotics.abm5954)