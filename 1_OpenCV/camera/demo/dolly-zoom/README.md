# Dolly Zoom效果

[toc]


​	Dolly Zoom又被称为希区柯克变焦，是一种在电影中常用的手法，常见于惊悚镜头.

​	其基本原理是$f$增加，物距也增加，保持前景物体成像尺寸不变，背景会感觉逐渐的拉近，原文叫large focal length compresses depth. 

## 基本原理

**电影中的例子**:

![空间扭曲的秘密-神奇的滑动变焦](./figures/Jaws.gif)

![空间扭曲的秘密-神奇的滑动变焦](./figures/VertigoEffect.gif)

![空间扭曲的秘密-神奇的滑动变焦](./figures/Goodfellas.gif)

不同焦距拍摄的同一物体

![这里写图片描述](./figures/20161106231138395)

可以看到在长焦镜头下，仿佛前景物体和背景较远处的物体距离被压缩了，即达到了对深度进行压缩的视觉效果.

**数学原理**:

![img](./figures/20161106231412365)

## 编译和运行

编译

```bash
sudo apt-get install libassimp-dev  # 解析3D模型
sudo apt-get install freeglut3-dev 
mkdir	build && cd build
cmake .. 
make
```

运行

```bash
./app ../data/scene.ply
```

+ w/s: 可以调整相机的位置

+ q: 激活dolly zoom，在移动相机时，焦距自动变化

![image-20200506155113729](./figures/image-20200506155113729.png)

## 参考资料

+ https://blog.csdn.net/uestcjidian/article/details/53057628
+ https://www.vmovier.com/31234/
+ http://dollyzoom.herokuapp.com/ `Dolly Zoom 在线演示`

