#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
using namespace cv;
using namespace std;

Mat dst, src, gray_src;
int t1_value = 20;
int max_value = 255;
const char* outputTitle = "output image";

int main(int argc, char **argv) {	
	const char* inputTitle  = "input image";
	
	src = imread("F:/opencv/test4.png");	
	if (!src.data) {
		cout<<"could not open image";
		return -1;
	}

	namedWindow(inputTitle,CV_WINDOW_AUTOSIZE);
	namedWindow(outputTitle, CV_WINDOW_AUTOSIZE);
	imshow(inputTitle, src);

	Canny(src, gray_src, 150, 200);
	cvtColor(gray_src,dst,CV_GRAY2BGR);
	imshow("edge image", gray_src);

	vector<Vec4f> plines;
	HoughLinesP(gray_src, plines, 1, CV_PI / 180.0, 10, 0, 10);
	Scalar color = Scalar(0, 0, 255);
	for (size_t i = 0; i < plines.size(); i++) {
		Vec4f hline = plines[i];
		line(dst, Point(hline[0], hline[1]), Point(hline[2], hline[3]), color, 3, LINE_AA);
	}
	imshow(outputTitle, dst);

	waitKey(0);
	return 0;
}

