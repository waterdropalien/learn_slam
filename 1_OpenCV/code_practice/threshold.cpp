#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;
Mat dst, src, gray_src;
int threshold_value = 127;
int threshold_max   = 255;
int typeValue = 2;
int typeMax = 4;

const char* inputTitle  = "input image";
const char* outputTitle = "output image";

void ThresholdDemo(int, void*);

int main(int argc, char **argv) {
	
	src = imread("F:/opencv/Sunny.jpg");	
	if (!src.data) {
		cout<<"could not open image";
		return -1;
	}
	namedWindow(inputTitle,CV_WINDOW_AUTOSIZE);
	namedWindow(outputTitle, CV_WINDOW_AUTOSIZE);
	imshow(inputTitle, src);
	createTrackbar("Threshold Value", outputTitle,&threshold_value,threshold_max, ThresholdDemo);
	createTrackbar("Type Value", outputTitle, &typeValue, typeMax, ThresholdDemo);
	ThresholdDemo(0, 0);

	waitKey(0);
	return 0;
}

void ThresholdDemo(int, void*) {
	cvtColor(src,gray_src,CV_BGR2GRAY);
	//threshold(gray_src,dst,threshold_value,threshold_max, typeValue);
	//�Զ�������ֵ
	//threshold(gray_src, dst, 0, 255, THRESH_OTSU| typeValue);
	threshold(gray_src, dst, 0, 255, THRESH_TRIANGLE | typeValue);
	imshow(outputTitle,dst);
}