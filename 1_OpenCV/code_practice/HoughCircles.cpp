#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
using namespace cv;
using namespace std;

Mat dst, src, gray_src;
int t1_value = 20;
int max_value = 255;
const char* outputTitle = "output image";

int main(int argc, char **argv) {	
	const char* inputTitle  = "input image";
	
	src = imread("F:/opencv/test5.png");	
	if (!src.data) {
		cout<<"could not open image";
		return -1;
	}

	namedWindow(inputTitle,CV_WINDOW_AUTOSIZE);
	namedWindow(outputTitle, CV_WINDOW_AUTOSIZE);
	imshow(inputTitle, src);

	Mat mouput;
	medianBlur(src, mouput, 3);
	cvtColor(mouput, mouput,CV_BGR2GRAY);

	vector<Vec3f> pcircles;
	HoughCircles(mouput, pcircles, CV_HOUGH_GRADIENT, 1, 10, 100, 30, 5, 50);
	src.copyTo(dst);
	for (size_t i = 0; i < pcircles.size(); i++) {
		Vec3f cc = pcircles[i];
		circle(dst, Point(cc[0], cc[1]), cc[2], Scalar(0, 0, 255), 2, LINE_AA);
		circle(dst, Point(cc[0], cc[1]), 2, Scalar(55, 100, 255), 2, LINE_AA);
	}
	imshow(outputTitle, dst);
	waitKey(0);
	return 0;
}

