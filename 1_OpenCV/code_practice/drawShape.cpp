#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat bgImage;
const char* drawdemoWin = "draw shaps and text demo";
void myLine(void);
void myRect(void);
void myEllipse(void);
void myCricle(void);
void myPoltgon(void);
void RandomLineDemo();


int main(int argc, char **argv) {

	bgImage = imread("F:/opencv/car.jpg");

	if (!bgImage.data) {
		cout<<"could not open image";
		return -1;
	}
	myLine();
	myRect();
	myEllipse();
	myCricle();
	myPoltgon();
	//putText(bgImage,"Hello OpenCv",Point(300,300),CV_FONT_HERSHEY_COMPLEX,2.0,Scalar(12,255,200),1,8);
	RandomLineDemo();

	//namedWindow(drawdemoWin,CV_WINDOW_AUTOSIZE);
	//imshow(drawdemoWin, bgImage);

	waitKey(0);
	return 0;
}

void myLine(void) {
	Point p1 = Point(20,30);
	Point p2;
	p2.x = 100;
	p2.y = 100;
	Scalar color = Scalar(0, 0, 255);
	line(bgImage,p1,p2,color,1,LINE_8);
}

void myRect(void) {
	Rect rect = Rect(200,100,300,300);
	Scalar color = Scalar(255,0,0);
	rectangle(bgImage,rect,color,2,LINE_4);
}

void myEllipse(void){
	Scalar color = Scalar(0,255,0);
	ellipse(bgImage,Point(bgImage.cols/2,bgImage.rows/2),Size(bgImage.cols/4,bgImage.rows/8),45,0,360,color,2,LINE_8);
}


void myCricle(void) {
	Scalar color = Scalar(255, 255, 0);
	circle(bgImage,Point(bgImage.cols /2,bgImage.rows /2),200,color,2,8);
}

void myPoltgon(void) {
	Point pts[1][5];
	pts[0][0] = Point(100, 100);
	pts[0][1] = Point(100, 200);
	pts[0][2] = Point(200, 200);
	pts[0][3] = Point(200, 100);
	pts[0][4] = Point(100, 100);

	const Point* ppts[5];
	ppts[0] = pts[0];

	int npt[] = { 5 };
	Scalar color = Scalar(255, 100, 255);

	fillPoly(bgImage,ppts,npt,1,color,8);

}

void RandomLineDemo() {
	RNG rng((unsigned)time(NULL));
	Point pt1;
	Point pt2;
	Mat bg = Mat::zeros(bgImage.size(),bgImage.type());
	namedWindow("random line demo",CV_WINDOW_AUTOSIZE);
	for (int i = 0; i < 10000; i++) {
		pt1.x = rng.uniform(0, bgImage.cols);
		pt1.y = rng.uniform(0, bgImage.cols);
		pt2.x = rng.uniform(0, bgImage.rows);
		pt2.y = rng.uniform(0, bgImage.rows);
		Scalar color = Scalar(rng.uniform(0,255), rng.uniform(0, 255), rng.uniform(0, 255));
		if (waitKey(50) > 0) {
			break;
		}
		line(bg,pt1,pt2,color,1,8);
		imshow("random line demo",bg);
	}

}