#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char **argv) {
	
	Mat dst, src;
	const char* inputTitle  = "input image";
	const char* outputTitle = "output image";
	src = imread("F:/opencv/test1.png");	
	//src = imread("F:/opencv/Sunny.jpg");
	if (!src.data) {
		cout<<"could not open image";
		return -1;
	}
	namedWindow(inputTitle,CV_WINDOW_AUTOSIZE);
	imshow(inputTitle, src);

	Mat kernel = getStructuringElement(MORPH_RECT,Size(11,11),Point(-1,-1));
	//morphologyEx(src, dst, CV_MOP_OPEN, kernel);��
	//morphologyEx(src, dst, CV_MOP_CLOSE, kernel);��
	//morphologyEx(src, dst, CV_MOP_GRADIENT, kernel);//�ݶ�
	//morphologyEx(src, dst, CV_MOP_TOPHAT, kernel);//��ñ
	morphologyEx(src, dst, CV_MOP_BLACKHAT, kernel);//��ñ
	imshow(outputTitle, dst);
	
	waitKey(0);
	return 0;
}

